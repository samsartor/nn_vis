from manim import *
import math
from julia import Main
import numpy as np
from itertools import product

Main.include('util.jl')

(model, starting_points, series_ids) = Main.generate(
    200, # samples
    100, # steps
    2 * math.pi, # arc angle
    3.0, # tan scale
    3, # dim
    "nn_3d_02.bson", # saved
)

colors = [RED, BLUE]

class NN3D(ThreeDScene):
    def __init__(self,
            layer_dur = 2.5,
            tlabel_dur = 0.5,
            post_dur = 1.0,
            colors = [RED, BLUE]):
        ThreeDScene.__init__(self)

        self.layer_dur = layer_dur
        self.tlabel_dur = tlabel_dur
        self.post_dur = post_dur
        self.colors = colors

        np.random.seed(420)
        darken = np.random.uniform(0, 0.3, len(starting_points))

        self.axes = ThreeDAxes()
        self.dots = [Dot(
            point=self.axes.coords_to_point(*point),
            radius=0.05,
            color=interpolate_color(self.colors[id], BLACK, d))
            for point, id, d in zip(starting_points, series_ids, darken)]
        self.dot_group = Group(*self.dots)

        self.add_fixed_orientation_mobjects(*self.dots)
        self.remove(*self.dots)

        self.index = 0

    def update_points(self, alpha):
        t = self.index + alpha
        (name, points) = Main.animate_points(model, starting_points, t)
        for dot, point in zip(self.dots, points):
            pos = self.axes.coords_to_point(*point)
            dot.move_to(pos)

    def animate_scatter(self, rate_func=rate_functions.smooth, **kwargs):
        return UpdateFromAlphaFunc(
            self.dot_group,
            lambda _, a: self.update_points(rate_func(a)),
            **kwargs)

    def retain_scatter(self, **kwargs):
        return UpdateFromFunc(self.dot_group, lambda _: self.update_points(0), **kwargs)

    def animate_scatter_all(self, delta):
        for _ in range(4):
            rate_func = lambda a: delta * rate_functions.smooth(a)
            self.play(self.animate_scatter(rate_func), run_time=self.layer_dur)
            self.index += delta

    def animate_scatter_all_text(self):
        text = None
        for _ in range(2):
            text1 = Text("Linear", color=BLUE)
            self.add_fixed_in_frame_mobjects(text1)
            self.remove(text1)
            text1.to_corner(UL)
            if text is None:
                text_ani = [Write(text1, run_time=self.tlabel_dur)]
            else:
                text_ani = [
                    Uncreate(text, run_time=self.tlabel_dur),
                    Create(text1, run_time=self.tlabel_dur)]
            self.play(*text_ani, self.animate_scatter(run_time=self.layer_dur))
            text = text1
            self.index += 1

            text2 = Text("Non-linear", color=RED)
            self.add_fixed_in_frame_mobjects(text2)
            self.remove(text2)
            text2.to_corner(UL)
            self.play(
                Uncreate(text, run_time=self.tlabel_dur),
                Create(text2, run_time=self.tlabel_dur),
                self.animate_scatter(run_time=self.layer_dur))
            text = text2
            self.index += 1

        self.play(Unwrite(text, run_time=self.tlabel_dur), Wait(self.post_dur))

    def swing_camera(self, full_cycle=12, angle=15):
        cam_time = 0
        def step_camera(theta, dt):
            nonlocal cam_time
            cam_time += dt
            theta.set_value(angle*DEGREES*math.sin(2 * math.pi * cam_time/full_cycle) - 45*DEGREES)

        self.renderer.camera.theta_tracker.add_updater(step_camera)
        self.add(self.renderer.camera.theta_tracker)

    def construct(self):
        self.set_camera_orientation(phi=0*DEGREES, theta=-90*DEGREES)
        self.axes.scale(0.5)
        self.play(Create(self.axes))
        self.play(AnimationGroup(*map(Create, self.dots), lag_ratio=0.05, run_time=4))
        self.wait(5)

        self.move_camera(phi=60*DEGREES, theta=-45*DEGREES,
            run_time=3, added_anims=[ApplyMethod(self.axes.scale, 2)])
        self.swing_camera()

        self.animate_scatter_all_text()
        self.layer_dur = 2
        self.animate_scatter_all(-1)
        self.animate_scatter_all_text()

        self.play(Uncreate(self.axes), *map(Uncreate, self.dots))

class OpeningLoop(NN3D):
    def __init__(self):
        NN3D.__init__(self, layer_dur=1)

    def construct(self):
        self.set_camera_orientation(phi=60*DEGREES, theta=45*DEGREES)
        self.swing_camera()

        self.play(
            *(Create(d) for d in self.dots),
            Create(self.axes),
            run_time=1.5)

        self.animate_scatter_all(1)
        self.animate_scatter_all(-1)
        self.animate_scatter_all(1)
        self.animate_scatter_all(-1)

        self.play(
            *(Uncreate(d) for d in self.dots),
            Uncreate(self.axes),
            run_time=1.5)

class WhyNN(Scene):
    def construct(self):
        self.wait(0.5)

        text = Group(*(Text(t).scale(1.5) for t in ['Why', 'Neural', 'Networks', '?']))
        text.arrange()
        self.play(AnimationGroup(*map(Write, text), lag_ratio=0.5), run_time=1)
        self.play(Create(Underline(text[1])))
        self.play(Create(Underline(text[2])))
        self.play(
            *map(Unwrite, text),
            Uncreate(self.mobjects[-2]),
            Uncreate(self.mobjects[-1]),
            run_time=0.5)

        self.wait(0.5)

class DiagramAnimation(NN3D):
    def __init__(self):
        NN3D.__init__(self,
            layer_dur=2,
            post_dur=0)

    def construct(self):
        self.set_camera_orientation(phi=0*DEGREES, theta=-45*DEGREES)

        fewaxes = self.axes.get_axes()
        manyaxes = [
            NumberLine(x_range=[-1.25, 1.25, 0.5], length=6, include_tip=True).rotate(theta*DEGREES)
            for theta in range(0, 180, 20)]
        self.play(AnimationGroup(*map(Write, manyaxes), run_time=3, lag_ratio=0.3))
        self.wait(2)
        self.move_camera(phi=60*DEGREES, run_time=3, added_anims=[
            AnimationGroup(*map(Unwrite, manyaxes[3:]), lag_ratio=0.3),
            *(ReplacementTransform(a, b) for a, b in zip(manyaxes, fewaxes))])

        self.wait(1)
        self.play(*(Create(dot) for dot in self.dots), run_time=2)

        self.play(ApplyMethod(self.axes.scale, 0.8), self.retain_scatter())
        self.play(ApplyMethod(self.axes.move_to, [-2.5, -2, 0]), self.retain_scatter())

        nodes = [
            [4, 2, 0],
            [2, 2, 0],

            [5, 0, 0],
            [3, 0, 0],
            [1, 0, 0],

            [3, -2, 0],
        ]

        signals = [Dot(point=n) for n in nodes]
        neurons = [Dot(point=n, fill_opacity=0, stroke_width=2, radius=0.3) for n in nodes]

        mtx1 = [Line(start=a, end=b).scale(0.6) for a, b in product(nodes[0:2], nodes[2:5])]
        mtx1b = [l.copy() for l in mtx1]
        mtx2 = [Line(start=a, end=b).scale(0.6) for a, b in product(nodes[2:5], nodes[5:6])]

        self.add_fixed_in_frame_mobjects(*signals, *neurons, *mtx1, *mtx1b, *mtx2)
        self.remove(*signals, *neurons, *mtx1, *mtx1b, *mtx2)

        self.wait(1)
        self.play(Indicate(fewaxes[0]))
        self.play(Indicate(fewaxes[1]))
        self.wait(0.5)
        self.play(*(Create(s) for s in signals[0:2]), run_time=1)
        self.wait(0.5)

        self.index = 0
        self.play(
            *(Create(s) for s in signals[2:5]),
            *(Create(l) for l in mtx1b),
            self.animate_scatter(),
            run_time=3)

        realmtx = DecimalMatrix([
            [-1.12,  1.27],
            [ 0.63,  1.82],
            [-1.82, -0.35]],
            element_to_mobject_config={"num_decimal_places": 2})
        self.add_fixed_in_frame_mobjects(realmtx)
        self.remove(realmtx)
        realmtx.scale(0.5)
        realmtx.next_to(signals[3], UP)

        self.play(
            *(Uncreate(l) for l in mtx1b),
            Write(realmtx),
            run_time=1.5)
        self.wait(2)
        self.play(
            *(Create(l) for l in mtx1),
            Unwrite(realmtx),
            run_time=1.5)
        self.wait(2)

        self.index = 1
        self.play(
            *(ReplacementTransform(s, n) for s, n in zip(signals[2:5], neurons[2:5])),
            self.animate_scatter(),
            run_time=self.layer_dur)

        self.index = 2
        self.play(
            *(Create(s) for s in signals[5:6]),
            *(Create(l) for l in mtx2),
            self.animate_scatter(),
            run_time=5)

        self.index = 3
        self.play(
            *(ReplacementTransform(s, n) for s, n in zip(signals[5:6], neurons[5:6])),
            self.animate_scatter(),
            run_time=2)

        self.wait(1)

        self.play(
            *(Uncreate(s) for s in signals[0:2]),
            *(Unwrite(n) for n in neurons[2:6]),
            *(Uncreate(l) for l in mtx1),
            *(Uncreate(l) for l in mtx2),
            *(Uncreate(d) for d in self.dots),
            Uncreate(self.axes))

class FastLoop(NN3D):
    def __init__(self):
        NN3D.__init__(self, layer_dur=8/4)

    def construct(self):
        self.set_camera_orientation(phi=60*DEGREES, theta=45*DEGREES)
        self.swing_camera(angle=10, full_cycle=9)

        self.play(
            *(Create(d) for d in self.dots),
            Create(self.axes),
            run_time=0.5)

        self.animate_scatter_all(1)

        self.play(
            *(Uncreate(d) for d in self.dots),
            Uncreate(self.axes),
            run_time=0.5)

class Thumbnail(NN3D):
    def construct(self):
        self.axes = ThreeDAxes(x_range=[-1, 2.5, 1], y_range=[-2.5, 1, 1], z_range=[-2, 2, 1])

        self.set_camera_orientation(phi=60*DEGREES, theta=-45*DEGREES)
        self.add(self.axes)
        self.add(*self.dots)

        self.axes.scale(0.5)
        self.axes.move_to(self.axes.c2p(1, -3, 0))

        self.index = 2
        self.update_points(0)

        nodes = [
            [4, 1, 0],
            [2, 1, 0],

            [5, -1, 0],
            [3, -1, 0],
            [1, -1, 0],

            [3, -3, 0],
        ]

        signals = [Dot(point=n) for n in nodes]
        neurons = [Dot(point=n, fill_opacity=0, stroke_width=2, radius=0.3) for n in nodes]

        mtx1 = [Line(start=a, end=b).scale(0.6) for a, b in product(nodes[0:2], nodes[2:5])]
        mtx2 = [Line(start=a, end=b).scale(0.6) for a, b in product(nodes[2:5], nodes[5:6])]

        self.add_fixed_in_frame_mobjects(*signals, *neurons, *mtx1, *mtx2)
        self.remove(*signals, *neurons, *mtx1, *mtx2)

        self.add(*signals[0:2])
        self.add(*mtx1)
        self.add(*neurons[2:6])
        self.add(*mtx2)

        self.add_fixed_in_frame_mobjects(Title("Neural Networks?", scale_factor=2.5))

