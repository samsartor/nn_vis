from manim import *
import math
import numpy as np
from itertools import product

class NeuronDiagram(Scene):
    def construct(self):
        nodes = [
            [1.5, 3, 0],
            [-1.5, 3, 0],

            [3, 0, 0],
            [0, 0, 0],
            [-3, 0, 0],

            [0, -3, 0],
        ]

        circles = [Dot(point=n, fill_opacity=0, stroke_width=2, radius=0.5) for n in nodes]
        neurons = [ImageMobject("content/neuron_head.png").move_to(n).set_height(1.6) for n in nodes]

        mtx1 = [Line(start=a, end=b).scale(0.6) for a, b in product(nodes[0:2], nodes[2:5])]
        mtx2 = [Line(start=a, end=b).scale(0.6) for a, b in product(nodes[2:5], nodes[5:6])]
        mtx = [*mtx1, *mtx2]

        self.wait(1)
        self.play(*map(Write, circles), run_time=2)
        self.play(*map(Create, mtx), run_time=2)

        self.wait(4)

        axons = [
            ImageMobject("content/axon.png")\
            .set(width=0.3)\
            .stretch_to_fit_height(1.5 * np.linalg.norm(l.get_vector()))\
            .move_to(l.get_center())
            .rotate(-math.atan2(l.get_vector()[0], l.get_vector()[1]))
            for l in mtx]

        self.play(*(FadeTransform(c, n) for c, n in zip(circles, neurons)), run_time=1)
        self.wait(1)
        self.add_foreground_mobjects(*neurons)
        self.play(*(FadeTransform(l, a) for l, a in zip(mtx, axons)), run_time=1)
        self.wait(1)

        self.wait(2)
        self.play(*map(FadeOut, self.mobjects), Write(Text("Artificial Neural Network")), run_time=0.5)
        self.wait(2)
        self.play(Unwrite(self.mobjects[0]), run_time=0.5)

def shorten(line, amt):
    len = np.linalg.norm(line.get_vector())
    line.set_length(len - amt)
    return line

class PixelsIntoNN(Scene):
    def construct(self):
        colors = np.array([
            [49,13,9],
            [140,106,85],
            [89,74,68],
            [65,35,24],
            [160,132,115],
            [121,104,102],
            [140,116,105],
            [188,176,169],
            [95,76,59]])

        colors = [rgb_to_color(c / 255) for c in colors]
        pixels = VGroup(*(Square(side_length=0.5, fill_opacity=1, stroke_width=0, color=c) for c in colors))

        cat = ImageMobject("content/cat.png").set_width(0.6*3)
        ecat = cat.copy()
        self.play(FadeIn(cat), run_time=1)
        self.wait(4)

        pixels.arrange_in_grid(3, 3, buff=0.1)
        self.play(FadeOut(cat), *map(FadeIn, pixels), run_time=1)
        self.wait(3)

        npixels = pixels.copy().arrange(buff=1).to_edge(UP)

        self.play(*(Transform(a, b) for a, b in zip(pixels, npixels)), run_time=1)
        self.wait(1)

        pivots = np.array([p.get_center() for p in pixels])
        mtx = Group(*(
            shorten(Line(start=a, end=b, stroke_width=1), 0.8)
            for a, b in product(pivots, pivots + 2*DOWN)))
        circles = Group(*(
            Circle(radius=0.3, color=WHITE).move_to(p)
            for p in pivots + 2*DOWN))

        self.play(
            AnimationGroup(*map(Create, mtx), lag_ratio=0.1),
            *map(Write, circles),
            run_time=6)

        circles2 = circles.copy().shift(2*DOWN)
        mtx2 = mtx.copy().shift(2*DOWN)
        self.play(
            AnimationGroup(*map(Create, circles2), lag_ratio=0.1),
            *map(Write, mtx2),
            run_time=3)

        epivots = np.array([[-3, pivots[0, 1], 0], [3, pivots[0, 1], 0]]) + 6*DOWN
        emtx = Group(*(
            shorten(Line(start=a, end=b, stroke_width=1), 0.8)
            for a, b in product(pivots + 4*DOWN, epivots)))
        ecircles = Group(*(
            Circle(radius=0.3, color=WHITE).move_to(p)
            for p in epivots))
        self.play(
            AnimationGroup(*map(Create, emtx), lag_ratio=0.1),
            *map(Write, ecircles),
            run_time=2)
        dog = Text("Dog", size=0.5).next_to(ecircles[1], DOWN)
        cat = Text("Cat", size=0.5).next_to(ecircles[0], DOWN)
        self.play(Write(cat), Write(dog), run_time=2)

        self.play(Indicate(cat), Indicate(ecircles[0]), run_time=2)

        pixels3 = pixels.copy().to_edge(DOWN)

        self.wait(1)
        self.play(*map(Uncreate, [
            cat, dog,
            *mtx,
            *circles,
            *mtx2,
            *circles2,
            *emtx,
            *ecircles]),
            Transform(pixels, pixels3),
            run_time=2)

        pixels4 = pixels.copy().arrange_in_grid(3, 3, buff=0.1).move_to(ORIGIN)
        self.play(*(Transform(a, b) for a, b in zip(pixels, pixels4)), run_time=2)
        self.wait(1)
        self.play(*map(FadeOut, pixels), FadeIn(ecat), run_time=2)
        self.play(FadeOut(ecat))
