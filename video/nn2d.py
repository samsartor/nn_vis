from manim import *
import math
from julia import Main
import numpy as np

Main.include('util.jl')

(model, starting_points, series_ids) = Main.generate(
    100, # samples
    100, # steps
    3/5 * math.pi, # angle
    4.0, # tan scale
    2, # dim
    "nn_2d_02.bson", # saved
)

colors = [RED, BLUE]

class NN2D(Scene):
    def __init__(self,
            show_text = True,
            tlabel_dur = 0.5,
            colors = [RED, BLUE]):
        Scene.__init__(self, always_update_mobjects=True)

        self.tlabel_dur = tlabel_dur
        self.colors = colors

        np.random.seed(420)
        darken = np.random.uniform(0, 0.3, len(starting_points))

        self.axes = Axes(x_range=[-2, 2, 0.5], y_range=[0, 2.5, 0.5])
        self.dots = [Dot(
            point=self.axes.coords_to_point(*point),
            radius=0.05,
            color=interpolate_color(self.colors[id], BLACK, d))
            for point, id, d in zip(starting_points, series_ids, darken)]
        self.dot_group = Group(*self.dots)

    def update_scatter(self, t):
        (name, points) = Main.animate_points(model, starting_points, t)
        for dot, point in zip(self.dots, points):
            pos = self.axes.c2p(*point)
            dot.move_to(pos)

    def animate_scatter(self, durs):
        index = 0
        def scatter_updater(_, alpha):
            nonlocal self, index
            self.update_scatter(index + rate_functions.smooth(alpha))

        text = None
        for _ in range(2):
            text1 = Text("Linear", color=BLUE, size=1).to_corner(UL)
            if text is None:
                text_ani = Write(text1, run_time=self.tlabel_dur)
                text = text1
            else:
                text_ani = Transform(text, text1, run_time=self.tlabel_dur)

            if durs[index] is not None:
                self.play(
                    text_ani,
                    UpdateFromAlphaFunc(self.dot_group, scatter_updater, run_time=durs[index]))
            index += 1

            text2 = Text("Non-linear", color=RED, size=1).to_corner(UL)
            if durs[index] is not None:
                self.play(
                    Transform(text, text2, run_time=self.tlabel_dur),
                    UpdateFromAlphaFunc(self.dot_group, scatter_updater, run_time=durs[index]))
            index += 1

        self.play(Unwrite(text, run_time=self.tlabel_dur), Wait(durs[index]))

    def construct(self):
        xaxis, yaxis = self.axes.get_axes()
        pivot = yaxis.get_center()
        pivot[0] = xaxis.number_to_point(-2)[0]
        yaxis.move_to(pivot)

        self.play(Create(self.axes), run_time=1)
        self.play(AnimationGroup(*map(Create, self.dots), lag_ratio=0.1, run_time=8))
        self.wait(2)

        self.wait(15)

        text1 = Tex(r"$\rightarrow$ Linear transforms").scale(0.75).to_edge(UP)
        text2 = Tex(r"$\rightarrow$ Non-linear transforms").scale(0.75).next_to(text1, DOWN)
        self.play(Write(text1), Write(text2))

        rectangle1 = Rectangle(height=1.4, width=0.6, fill_opacity=1, stroke_width=4).to_corner(UR)
        rectangle2 = rectangle1.copy().shift(LEFT)
        self.play(Write(rectangle1), Write(rectangle2))
        self.wait(3)
        self.play(Unwrite(rectangle1), Unwrite(rectangle2))

        new_axes = Axes()
        self.play(
            Unwrite(text1),
            Unwrite(text2),
            ReplacementTransform(self.axes, new_axes),
            *(ApplyMethod(d.move_to, new_axes.c2p(*p)) for d, p in zip(self.dots, starting_points)))
        self.axes = new_axes

        self.animate_scatter([4, 4, 5, None, 0.5])
        self.play(UpdateFromAlphaFunc(
            self.dot_group,
            lambda _, t: self.update_scatter(3-3*rate_functions.smooth(t))),
            run_time=3)
        self.wait(0.5)
        self.animate_scatter([3, 3, 3, 6, 0.5])

        for index in range(4, 0, -1):
            self.play(UpdateFromAlphaFunc(
                self.dot_group,
                lambda _, a: self.update_scatter(index - rate_functions.smooth(a))),
                run_time=2)

        self.wait(3)

        self.play(Uncreate(self.axes), *map(Uncreate, self.dots))

