using LinearAlgebra
using Flux
import Flux.Optimise
import Random: shuffle, MersenneTwister
using BSON: @load, @save

function generate_table(N, angle)
    rng = MersenneTwister(69)
    ts = rand(rng, Float64, (N, 2)) .* angle .- angle/2
    xs = [1.8 1.2] .* sin.(ts) .+ 0.1 * randn(rng, (N, 2))
    ys = [1.8 1.2] .* cos.(ts) .+ 0.1 * randn(rng, Float64, (N, 2))
    table = [xs[:, 1] ys[:, 1] zeros(N) zeros(N); xs[:, 2] ys[:, 2] zeros(N) ones(N)]
    table = table[sortperm([ts[:, 1]; ts[:, 2]]), :]
    return table
end

function two_layer_tanh(table, steps, dim)
    model = Chain(
        Dense(dim, dim, NNlib.tanh),
        Dense(dim, dim, NNlib.tanh),
    )

    loss(x, y) = sum((model(x)[:, 1] .- y) .^ 2)

    for _ in 1:steps
        inds = shuffle(1:size(table, 1))
        Optimise.train!(
            loss,
            params(model),
            [(r[1:dim], r[4]*2 - 1) for r in eachrow(table[inds, :])],
            Optimise.ADAM(0.02),
        )
    end

    return model
end

function lerp(a, b, t)
    return (1-t)*a + t*b
end

# based on https://en.wikipedia.org/wiki/Singular_value_decomposition#Singular_values_as_semiaxes_of_an_ellipse_or_ellipsoid
function mlerp(m, t)
    r = svd(m)
    U = r.U
    S = r.S
    V = r.V

    # for each basis vector
    for i in 1:size(m, 2)
        # basis is moving from v -> u
        v = V[:, i]
        u = U[:, i]

        # if the angle of rotation is >90°, normalize by reflecting the basis
        d = dot(u, v)
        if d < 0
            U[:, i] *= -1
            S[i] *= -1
            d *= -1
        end

        # angle of motion
        Ω = acos(d)

        # if the basis "length" goes negative, change to 180° rotation
        if S[i] < 0
            U[:, i] *= -1
            S[i] *= -1
            Ω = pi - Ω
        end

        # slerp the basis vectors
        U[:, i] = (sin((1-t)*Ω) * v + sin(t*Ω) * u) / sin(Ω)

        # lerp the basis vector length
        S[i] = lerp(1, S[i], t)
    end
    # recompose the matrix
    return U * Diagonal(S) * r.Vt
end

function decompose_model(model, tan_scale)
    r_tan_scale = 1/tan_scale

    function decompose_op(layer)
        return [
            ("linear", (x, t) -> t * r_tan_scale * layer.bias + mlerp(r_tan_scale * layer.W, t) * x),
            ("squash", (x, t) -> lerp(x, tanh.(tan_scale .* x), t)),
        ]
    end

    return vcat([decompose_op(model[i]) for i in 1:length(model)]...)
end

function animate_points(decomposed, points, t)
    t = clamp(t, 0, length(decomposed))

    i = 0
    while t - i > 1
        (name, f) = decomposed[i + 1]
        points = mapslices(x -> f(x, 1), points, dims=2)
        i += 1
    end

    (name, f) = decomposed[i + 1]

    last_t = t - i
    # last_t = clamp(0.50 * tanh(last_t * 6 - 3) + 0.5, 0, 1)

    points = mapslices(x -> f(x, last_t), points, dims=2)
    return (name, points)
end

function generate(num, steps, angle, tan_scale, dim, saved)
    table = generate_table(num, angle)
    if saved == nothing
        model = two_layer_tanh(table, steps, dim)
        @save "nn_checkpoint.bson" model
    else
        @load saved model
    end
    model = decompose_model(model, tan_scale)
    return (model, table[:, 1:dim], Int.(table[:, 4]))
end

